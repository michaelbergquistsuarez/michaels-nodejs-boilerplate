# Michaels node.js boilerplate

## Introduction
This repository is a boilerplate for new node.js projects.

### Important information
This repository has **not** been approved for public use.

## Usage
These instructions all require you to enter commands within your operating system's console or terminal and entering the described commands.

### Download
To download a copy of this repository.

``git clone https://github.com/MichaelBergquistSuarez/michaels-nodejs-boilerplate ~/michaels-nodejs-boilerplate``

### Navigate to the repository.
Navigate to the downloaded repository to be enable to use the commands below. 

``cd ~/michaels-nodejs-boilerplate``

### Install core files
To install the npm module dependencies.

``npm install``

### Run
To start the application.

``npm start``

## File descriptions
### index.js
The default main file.

### package.json
A configuration file for node.js. Read the [npm documentation at the package.json section](https://docs.npmjs.com/files/package.json) or the [npm getting started guide at the package.json section](https://docs.npmjs.com/getting-started/using-a-package.json).

## Dependencies
### Enviroment
#### [node.js](https://developers.google.com/v8/)
A javascript runtime running [Chrome V8](https://developers.google.com/v8/).

#### [npm](https://www.npmjs.com/)
A package manager for node.js

### npm modules
#### [debug](https://www.npmjs.com/package/debug)
Tiny node.js debugging utility modelled after node core's debugging technique.

##### Simple usage
Prepend a node call with `DEBUG=` followed by the namespace to debug. Wildcards are allowed.
Example:
``DEBUG=* node index``

#### [lodash](https://www.npmjs.com/package/debug)
The [modern build](https://github.com/lodash/lodash/wiki/Build-Differences) of [lodash](https://lodash.com/) with packages for [Bower](http://bower.io/), [Component](http://component.github.io/), & [Volo](http://volojs.org/).